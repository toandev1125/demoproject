export const state = () => ({
  product: {},
  bestSeller: {},
  sale: {},
  featured: {},
  mostView: {}
})

export const mutations = {
  addProduct(state, data) {
    state.product = data
  },
  loadComment(state, data) {
    state.product.comments.comments = state.product.comments.comments.concat(data.comments)
    state.product.comments.next = data.next
  },
  setBestSeller(state, data) {
    state.bestSeller = data
  },
  setSale(state, data) {
    state.sale = data
  },
  setFeatured(state, data) {
    state.featured = data
  },
  setMostView(state, data) {
    state.mostView = data
  }
}
