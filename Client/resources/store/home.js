export const state = () => ({
  products: {}
})

export const mutations = {
  SET_PRODUCTS(state, data) {
    state.products = data
  }
}

export const actions = {
  setProducts({commit}, data) {
    commit('SET_PRODUCTS', data)
  }
}
