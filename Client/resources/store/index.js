const cookieparser = require('cookieparser')

export const state = () => ({
  auth: null
})

export const mutations = {
  setAuth(state, auth) {
    state.auth = auth
  }
}

export const actions = {
  nuxtClientInit({ commit, state, dispatch }, { req }) {
    let auth = null
    if (document.cookie) {
      const parsed = cookieparser.parse(document.cookie)
      try {
        auth = parsed.auth
      } catch (err) {
        // No valid cookie found
      }
    }
    commit('setAuth', auth)
  }
}