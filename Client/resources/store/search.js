export const state = () => ({
  data: {}
})

export const mutations = {
  SET_DATA(state, data) {
    state.data = data
  }
}

export const actions = {
  setData({commit}, data) {
    commit('SET_DATA', data)
  }
}
