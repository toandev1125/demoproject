export const state = () => ({
  type: '',
  token: ''
})

export const mutations = {
  setLogin(state, {type, token}) {
    state.type = type
    state.token = token
  }
}