export const state = () => ({
  state: [],
  city: []
})

export const mutations = {
  addState(state, listState) {
    state.state = listState
  },
  addCity(state, listCity) {
    state.city = listCity
  }
}