import { currency } from '@/js/Utils/Utils'

export default class Cart {
  constructor(ctx) {
    this.ctx = ctx
  }

  get() {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.get('carts').then(res => {
        resolve(res.data)
      }).catch(err => {
        reject()
      })
    })
  }

  add(idProduct, size, color, quantity) {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.post('carts', {
        idProduct, size, color, quantity
      }).then((res) => {
        this.count()
        this.ctx.$notify({
          title: 'Notification',
          text: 'Added to the cart successfully',
          type: 'success'
        })
        resolve(res.data)
      }).catch((err) => {
        const { data, status } = err.response
        if(status === 400) {
          switch(data.data.state) {
            case 1:
              this.ctx.$notify({
                title: 'Notification',
                text: 'The number of products in the cart has exceeded the existing quantity',
                type: 'warn'
              })
              break
            case 2:
              this.ctx.$notify({
                title: 'Notification',
                text: 'This product is out of stock',
                type: 'error'
              })
              break
            case 3:
              this.ctx.$notify({
                title: 'Notification',
                text: 'The Product weight exceed the limit of our shipping carrier allow. Please call this phone number for further assistance',
                type: 'error'
              })
              break
            case 4:
              this.ctx.$notify({
                title: 'Notification',
                text: 'Please select quantity more than 0',
                type: 'error'
              })
              break
            default:
          }
        }
        reject()
      })
    })
  }

  check() {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.post('cartcheck').then(res => {
        resolve(res)
      }).catch(err => {
        reject(err.response)
      })
    })
  }

  count() {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.get('cartscount')
        .then((res) => {
          this.ctx.$store.commit('cart/setCount', res.data.data)
          resolve(res.data)
        })
        .catch((err) => {
          resolve(0)
        })
    })
  }

  changeQuantity(index, quantity) {
    console.log(quantity)
    return new Promise((resolve, reject) => {
      const { id, size, color } = this.ctx.$store.state.cart.cart.products[index]
      this.ctx.$axios.put('carts', {
        idProduct: id,
        size, color,
        quantity
      }).then((res) => {
        this.ctx.$store.commit('cart/changeQuantity', {
          index,
          quantity,
          stateCustom: 0,
          max: res.data.data.max
        })
        resolve(res.data)
      }).catch((err) => {
        const { data, status } = err.response
        if(status === 400) {
          this.ctx.$store.commit('cart/changeQuantity', {
            index,
            quantity,
            stateCustom: data.data.state,
            max: data.data.max
          })
        }
        resolve()
      })
    })
  }

  remove(index) {
    return new Promise((resolve, reject) => {
      const { id, size, color } = this.ctx.$store.state.cart.cart.products[index]
      this.ctx.$axios.delete('cartsremove', {
        data: {
          idProduct: id,
          size, color
        }
      }).then((res) => {
        this.ctx.$store.commit('cart/removeCart', index)
        this.count()
        this.ctx.$notify({
          title: 'Notification',
          text: 'Removed product from cart',
          type: 'success'
        })
        resolve(res.data)
      }).catch((err) => {
        resolve()
      })
    })
  }

  useCoupon(coupon_code, use) {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.post('coupon_code', {
        coupon_code,
        use
      }).then(res => {
        resolve(res)
      }).catch(err => {
        reject(err.response)
      })
    })
  }

  removeCoupon(coupon_code) {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.delete('coupon_code_del', {
        data: {
          coupon_code
        }
      }).then(res => {
        resolve(res)
      }).catch(err => {
        reject(err.response)
      })
    })
  }

  calculator(subTotal, condition) {
    let couponPrice = 0
    let { total_orders, price, percent, amount_reduced } = condition

    if(total_orders) {
      if(subTotal < total_orders) {
        return {
          message: `The sub total amount must be greater than ${currency(total_orders)}`,
          status: false
        }
      }
    }

    if(price) {
      couponPrice = price <= subTotal ? price : subTotal
    } else {
      if(couponPrice === 0 && percent) {
        if(percent > 100) {
          percent = 100
        }
  
        let percentPrice = (subTotal * percent / 100)
  
        if(amount_reduced && amount_reduced < percentPrice) {
          percentPrice = amount_reduced
        }
  
        if(percentPrice <= subTotal) {
          couponPrice = percentPrice
        }
      } else {
        return {
          message: `Cannot use this coupon`,
          status: false
        }
      }
    }

    return {
      couponPrice,
      status: true
    }
  }

  changeAddress(id) {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.get(`shippingrate?idaddress=${id}`).then(res => {
        resolve(res.data)
      }).catch(err => {
        reject()
      })
    })
  }

  checkShipping({idAddress, idRate, idShipment, email, carrier, idInvoice}) {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.post('shippingcheck', {
        idAddress, idRate, idShipment, email, carrier, idInvoice
      }).then(res => {
        resolve(res)
      }).catch(err => {
        reject(err.response)
      })
    })
  }
}