export default [
  {
    name: 'my-profile',
    component: 'MyProfile',
    title: 'My Profile',
    auth: true
  },
  {
    name: 'change-password',
    component: 'ChangePassword',
    title: 'Change Password',
    hidden: true,
    auth: true
  },
  {
    name: 'address-book',
    component: 'AddressBook',
    title: 'Address Book',
    auth: true
  },
  {
    name: 'vouchers',
    component: 'Vouchers',
    title: 'Vouchers',
    auth: true
  },
  {
    name: 'my-orders',
    component: 'MyOrders',
    title: 'My Orders',
    auth: true
  },
  {
    name: 'my-reviews',
    component: 'MyReviews',
    title: 'My Reviews',
    auth: true
  },
  {
    name: 'not-yet-rated',
    component: 'NotYetRated',
    title: 'Not Yet Rated',
    auth: true
  },
  {
    name: 'my-wishlist',
    component: 'MyWishList',
    title: 'My Wishlist',
    auth: true
  },
  {
    name: 'view-history',
    component: 'ViewHistory',
    title: 'View History',
    auth: false
  }
]