import numeral from 'numeral'
import slugify from 'slugify'

export const checkEmail = (email) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export const currency = (num) => {
  return numeral(num).format('$0,0.00')
}

export const slugifyConvert = (str) => {
  return slugify(str.replace(/\/|\?/g, ""), {
    replacement: '-',
    remove: /[*+~.()'"!:@]/g,
    lower: true
  })
}