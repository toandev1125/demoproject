export default function ({ store, redirect }) {
  if(store.state.cart.step === 1) {
    return redirect('/cart')
  }
}