'use strict'
const Easypost = require('@easypost/api')
const api = new Easypost('EZTK99b24f80d9a1432a833741ccf6c91f4bOkP9X2IjwPuNttghSaQZ9g')

class ShippingController {
  async calculator({ request, response}) {
    const fromAddress = new api.Address({
      company: 'EasyPost',
      street1: '417 Montgomery Street',
      street2: '5th Floor',
      city: 'San Francisco',
      state: 'CA',
      zip: '94104',
      phone: '415-528-7555'
    })

    const toAddress = new api.Address({
      name: 'George Costanza',
      company: 'Vandelay Industries',
      street1: '1 E 161st St.',
      city: 'Bronx',
      state: 'NY',
      zip: '10451'
    })

    const parcel = new api.Parcel({
      length: 9,
      width: 6,
      height: 2,
      weight: 10,
    })

    const shipment = new api.Shipment({
      to_address: toAddress,
      from_address: fromAddress,
      parcel: parcel
    })

    const result = await shipment.save()
    console.log(result)

    return response.send(result)

    // return response.send({
    //   rates: result.rates.map(item => {

    //     return {
    //       id: item.id,
    //       service: item.service,
    //       rate: item.rate,
    //       carrier: item.carrier
    //     }
    //   })
    // })
  }
}

module.exports = ShippingController
